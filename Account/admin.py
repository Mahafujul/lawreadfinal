from django.contrib import admin

# Register your models here.
from Account.models import Role, User

admin.site.register(Role)
admin.site.register(User)
