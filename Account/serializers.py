from rest_framework import serializers

from Account.models import User, Role


class UserSerialzer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = '__all__'

    def create(self, validated_data):
        user = User(
            first_name = validated_data['first_name'],
            last_name = validated_data['last_name'],
            mobile_number = validated_data['mobile_number'],
            email = validated_data['email'],
            role_id = validated_data['role_id']
        )

        user.set_password(validated_data['password'])
        user.save()
        return user
class RoleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Role
        fields = '__all__'
