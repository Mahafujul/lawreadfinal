from django.core.exceptions import ValidationError
from django.shortcuts import render

# Create your views here.
from rest_framework.response import Response
from rest_framework import status
from rest_framework.decorators import api_view

from Account.models import User
from Account.serializers import UserSerialzer, RoleSerializer


@api_view(['POST'])
def user_register(request):
    if not request.data:
        raise ValidationError("Invalid Access")
    # mobile_number =User.objects.filter(mobile_number = request.data['mobile_number'])
    # email= User.objects.filter(email = request.data['email'])
    #
    # if len(mobile_number)!=0:
    #     return ValidationError("This email id is already registered.")
    # if len(email)!=0:
    #     return ValidationError("This mobile number is already registered")

    serializer = UserSerialzer(data=request.data)
    if serializer.is_valid():
        serializer.save()

        result = {"User":serializer.data}
        return Response(result, status = status.HTTP_201_CREATED)
    else:
        result = {"Data":serializer.errors}
        return Response(result, status = status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def role_post(request):
    if not request.data:
        raise  ValidationError("Invalid Access")

    serializer = RoleSerializer(data= request.data)
    if serializer.is_valid():
        serializer.save()
        result = {"Role":serializer.data}
        return Response(result, status=status.HTTP_201_CREATED)
    else:
        result = {"Error":serializer.errors}
        return Response(result, status=status.HTTP_400_BAD_REQUEST)
