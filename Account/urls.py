from django.urls import path
from . import views


urlpatterns = [
    path('user_register',views.user_register),
    path('role_post', views.role_post)
]
