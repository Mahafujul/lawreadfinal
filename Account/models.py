from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.db import models

# Create your models here.


class Role(models.Model):
    role_name = models.CharField(max_length=50)
    created_date_time = models.DateTimeField(auto_now_add=True)
    updated_date_time = models.DateTimeField(auto_now_add=True)


class MyUserManager(BaseUserManager):
    def create_user(self, first_name, last_name, mobile_number, email, role_id, password):
        if not mobile_number:
           raise ValueError("User must have mobile number.")
        if not email:
            raise ValueError ("User must have email address.")

        role_id_value = Role.objects.get(id = role_id)

        user = self.model(
            email = self.normalize_email(email),
            first_name = first_name,
            last_name = last_name,
            mobile_number = mobile_number,
            role_id = role_id_value
        )
        user.set_password(password)
        user.save(using=self._db)
        return user


    # def create_superuser(self, first_name, last_name, mobile_number, email, role_id, password ):
    #
    #     role_id_value = Role.objects.get(id = role_id)
    #     user = self.create_user(
    #         first_name = first_name,
    #         last_name = last_name,
    #         mobile_number= mobile_number,
    #         email = email,
    #         role_id = role_id_value,
    #         password = password
    #     )
    #     user.is_admin = True
    #     user.save(using=self._db)
    #
    #     return user



class User(AbstractBaseUser):
    first_name = models.CharField(max_length=250)
    last_name = models.CharField(max_length=250)
    mobile_number = models.CharField(max_length=10,unique=True)
    email = models.EmailField(verbose_name='email address', max_length=250,unique= True)
    password = models.CharField(max_length=250)
    role_id = models.ForeignKey(Role,on_delete = models.CASCADE)
    create_date_time = models.DateTimeField(auto_now_add=True)
    updated_date_time = models.DateTimeField(auto_now_add=True)

    object = MyUserManager()

    USERNAME_FIELD = ['mobile_number','password']
    REQUIRED_FIELDS = ['first_name', 'last_name','mobile_number','email', 'role_id']

    def __str__(self):
        return self.email



# class UserRef(models.Model):
#     created_by = models.ForeignKey(User, on_delete=models.CASCADE)
#     updated_by = models.ForeignKey(User, on_delete=models.CASCADE)
#     created_date_time = models.DateTimeField()
#     updated_date_time = models.DateTimeField()


