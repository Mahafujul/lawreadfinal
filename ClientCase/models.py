from django.db import models

# Create your models here.

# class ClientCase(UserRef):
#     case_number = models.IntegerField()
#     first_name = models.CharField(max_length=250)
#     last_name = models.CharField(max_length=250)
#     mobile_number = models.IntegerField()
#     email = models.EmailField()
#     start_date = models.DateField()
#     next_date = models.DateField()
#     user_id = models.ForeignKey(User, on_delete=models.CASCADE)
#     state_id = models.ForeignKey(State, on_delete=models.CASCADE)
#     district_id = models.ForeignKey(District, on_delete=models.CASCADE)
#     status_id = models.ForeignKey(Status, on_delete=models.CASCADE)
#     court_category_id = models.ForeignKey(CourtCategory, on_delete=models.CASCADE)
#     court_id = models.ForeignKey(Court, on_delete=models.CASCADE)
#     court_type_id = models.ForeignKey(CourtType, on_delete=models.CASCADE)

#
# class ClientCasePreviousReferenceDate(UserRef):
#     client_case_id = models.ForeignKey(ClientCase, on_delete=models.CASCADE)
#     previous_date = models.DateField()

# class ClientCasePreviousReferenceCourt(UserRef):
#     client_case_id = models.ForeignKey(ClientCase, on_delete=models.CASCADE)
#     state_id = models.ForeignKey(State, on_delete=models.CASCADE)
#     district_id = models.ForeignKey(District, on_delete=models.CASCADE)
#     court_category_id = models.ForeignKey(CourtCategory, on_delete=models.CASCADE)
#     court_id = models.ForeignKey(Court, on_delete=models.CASCADE)
#
# class ClientCaseDocuments(UserRef):
#     client_case_id = models.ForeignKey(ClientCase, on_delete=models.CASCADE)
#     title = models.CharField(max_length=250)
#     file_path = models.FileField()
