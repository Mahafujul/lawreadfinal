# from django.db import models
# from Account.models import UserRef
# # Create your models here.
#
#
# class LawyerInfo(UserRef):
#     headline = models.CharField(max_length=50)
#     sub_headline = models.CharField(max_length=50)
#     description = models.TextField()
#     gender_id = models.ForeignKey(Gender, on_delete=models.CASCADE)
#     photo = models.ImageField()
#     user_id = models.ForeignKey(User, on_delete=models.CASCADE)
#
#
#
# class Education(UserRef):
#     school_name = models.CharField(max_length=250)
#     university = models.CharField(max_length=250)
#     degree_name = models.CharField(max_length=250)
#     start_year = models.DateField()
#     end_year = models.DateField()
#     is_studying = models.BooleanField()
#     user_id = models.ForeignKey(User, on_delete=models.CASCADE)
#
# class Experience(UserRef):
#     title = models.CharField(max_length=250)
#     headline = models.CharField(max_length=250)
#     description = models.CharField(max_length=250)
#     employement_type_id = models.ForeignKey(EmployeType, on_delete=models.CASCADE)
#     company_name = models.CharField(max_length=250)
#     location = models.CharField(max_length=250)
#     start_year = models.DateField()
#     end_year = models.DateField()
#     user_id = models.ForeignKey(User, on_delete=models.CASCADE)
#
# class ContactInfo(UserRef):
#     mobile_number = models.IntegerField()
#     email = models.EmailField()
#     address_id = models.ForeignKey(Address, on_delete=models.CASCADE)
#     user_id = models.ForeignKey(User, on_delete=models.CASCADE)
#     contact_type_id = models.ForeignKey(abc, on_delete=models.CASCADE)
#
